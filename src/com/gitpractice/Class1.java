/**
 *
 */
package com.gitpractice;

/**
 * @author terminator637
 *
 */
public class Class1 {
	private String name;
	private int number;

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
