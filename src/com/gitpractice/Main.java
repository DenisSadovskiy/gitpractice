/**
 *
 */
package com.gitpractice;

import java.util.logging.Logger;

/**
 * @author terminator637
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Class1 class1 = new Class1();
		class1.setName("gitpractice_conflict2");
		Logger logger = Logger.getLogger(Main.class.getName());
		String class1Name = class1.getName();
		logger.info(class1Name);
	}

}
